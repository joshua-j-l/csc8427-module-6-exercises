import java.util.Locale;
import java.util.Random;

public class DateConverter {

    public static void main(String[] args) {

        Random rand = new Random();

        String[] months = { "Jan", "february", "march", "apr", "May", "June",
                "July", "august", "Sep", "oct", "November", "December" };

        String[] rawMonths = { "Jan", "february", "march", "apr", "May", "June",
                "July", "august", "Sep", "oct", "November", "December" };

        String cleanedMonths[] = new String[12];

        for( String month: rawMonths){
            int counter = 0;
            cleanedMonths[counter] = month.toLowerCase(Locale.ROOT).substring(0,3);
            counter = counter + 1;
        }

        for (String month : cleanedMonths){
            int printCounter = 0;
            System.out.println(cleanedMonths[printCounter]);
            printCounter = printCounter + 1;
        }


//        String testClean = rawMonths[10].toLowerCase(Locale.ROOT).substring(0,3);
//        System.out.println(testClean);

        int random = rand.nextInt(11);
        random += 1;

        String selectedMonth = months[random];
        System.out.println("Selected month is: " + selectedMonth);
        int numericalMonth = 0;

        switch (selectedMonth)
        {
            case "January":
            case "Jan":
                numericalMonth = 1;
                break;
            case "February":
            case "february":
                numericalMonth = 2;
                break;
            case "March":
            case "march":
                numericalMonth = 3;
                break;
            case "April":
            case "apr":
                numericalMonth = 4;
                break;
            case "May":
                numericalMonth = 5;
                break;
            case "June":
                numericalMonth = 6;
                break;
            case "July":
                numericalMonth = 7;
                break;
            case "August":
            case "august":
                numericalMonth = 8;
                break;
            case "September":
            case "Sep":
                numericalMonth = 9;
                break;
            case "October":
            case "oct":
                numericalMonth = 10;
                break;
            case "November":
                numericalMonth =11;
                break;
            case "December":
                numericalMonth = 12;
                break;
            default:
                System.out.println("Beep boop invalid entry");
                break;
        }
        System.out.println("The numerical value for the selected month is: " + numericalMonth);
    }
}
