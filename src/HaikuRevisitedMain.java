public class HaikuRevisitedMain {

    public static void main(String[] args) {

        String[][] strings = {
                {"An", "Old", "Silent", "Pond..."},
                {"A", "frog", "jumps", "into", "the", "pond,"},
                {"splash!", "Silence", "again."}
        };

        for (String i[] : strings) {
            for (String j : i) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
    }
}

//    Original code
//        for(int i=0;i<strings.length;i++){
//        for(int j=0;j<strings[i].length;j++){
//        System.out.print(strings[i][j]+" ");     // print each word plus a [space] character
//        }
//        System.out.println();                            // new line at the end of each row
//        }


//https://www.youtube.com/watch?v=4LZc6WtloIc