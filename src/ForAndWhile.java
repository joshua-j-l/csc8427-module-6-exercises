public class ForAndWhile {

    public static void main(String[] args) {

        String[] months = {"January", "February", "March", "April", "May", "June ",
                "July", "August", "September", "October", "November", "December"};

        //while loop
        int whileCounter = 0;
        boolean foundStatus = false;
        while (whileCounter < months.length) {
            if (months[whileCounter] == "January") {
                System.out.println("Match Found");
                foundStatus = true;
                break;
            } else {
                whileCounter++;
            }
        }
        if (foundStatus == false) {
            System.out.println("While: Searched value not found in list :(");
        }

        //for loop
        int ifCounter = 0;
        boolean ifFoundStatus = false;
        for (String month : months){
            if (months[ifCounter] == "January") {
                System.out.println("Match Found");
                ifFoundStatus = true;
                break;
            } else {
                ifCounter++;
            }
        }
        if (ifFoundStatus == false) {
            System.out.println("For: Searched value not found in list :(");
        }
    }
}
